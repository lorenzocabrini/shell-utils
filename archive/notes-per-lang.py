#! /usr/bin/env python3

# notes-by-lang: prints the number of notes by language
#
# I use this to see which languages I need to add more notes to. I used to create
# a deck per language, but that got a bit cumbersome, especially since I ended up
# with many decks with just a few notes in them. So now I maintain a single deck
# for all languages I study and I tag each note with the language.
#
# Dependencies:
#  - requests (python)
#  - colorama (python)
#  - AnkiConnect (Anki add-on)
#
# In order to use the script, you are expected to have:
#  - each card tagged with the language that card relates to
#  - a file with all the language tags in ~/.anki-language-tags, one per line

import sys
import requests
import json
import os
from colorama import Fore, Style

# The URL of AnkiConnect. This should match the AnkiConnect configuration.
ANKI_URL = 'http://localhost:8765'

# The langauges file.
LANG_FILE = '{}/.anki-language-tags'.format(os.getenv('HOME'))


def note_count_by_tag(tag):
    payload = {
        'version': 6,
        'action': 'findNotes',
        'params': {
            'query': f'tag:{tag}',
        }
    }

    try:
        response = requests.post(ANKI_URL, data=json.dumps(payload)).json()
    except requests.exceptions.ConnectionError:
        print("Unable to connect with Anki. Is it running?")
        sys.exit(1)

    if len(response) != 2:
        raise Exception('response has an unexpected number of fields')
    if 'error' not in response:
        raise Exception('response is missing required error field')
    if 'result' not in response:
        raise Exception('response is missing required result field')
    if response['error'] is not None:
        raise Exception(response['error'])
    return len(response['result'])


stats = {}
total = 0
non_empty = 0

try:
    with open(LANG_FILE) as file:
        languages = list(filter(
            len,
            [line.strip() for line in file.readlines()]
        ))
except FileNotFoundError:
    print("Tags file not found")
    sys.exit(1)

for lang in languages:
    count = note_count_by_tag(lang)
    stats[lang] = count
    total += count
    if count > 0:
        non_empty += 1

average = total / float(len(languages))
average_ne = total / float(non_empty)

print(f"{'Total Cards':35}{Fore.BLUE}{total:5}{Style.RESET_ALL}")
print(f"{'Average':35}{Fore.BLUE}{average:8.2f}{Style.RESET_ALL}")
print(f"{'Average (non-empty)':35}{Fore.BLUE}{average_ne:8.2f}{Style.RESET_ALL}")
print(78 * '-')

for i, lang in enumerate(sorted(stats, key=stats.get, reverse=True), 1):
    stat = stats[lang]
    perc = stat / total * 100.0
    if stats[lang] < average:
        color = Fore.RED
    else:
        color = Fore.GREEN
    print(f"{i:3}. {lang:30}{color}{stat:5}{perc:15.1f}%{Style.RESET_ALL}")
