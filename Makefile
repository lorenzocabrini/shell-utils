PROG = notes-by-lang
INSTALL_DIR = ~/.local/bin

all: build install

build:
	cargo build --release

install:
	cp target/release/$(PROG) $(INSTALL_DIR)