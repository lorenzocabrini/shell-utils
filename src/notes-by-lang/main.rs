use colored::*;
use dirs::home_dir;
use serde_json::{json, Value};
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

const TAG_FILE: &str = ".anki-language-tags";

struct Language {
    tag: String,
    count: u32,
}

fn main() {
    let mut languages: Vec<Language> = vec![];
    let file = File::open(tags_file()).expect("Tags file not found");
    for line in parse_file(file) {
        match anki(&line) {
            Ok(item) => {
                languages.push(Language {
                    tag: line,
                    count: item["result"].as_array().unwrap().len() as u32,
                });
            }

            Err(_) => {
                eprintln!("{}", "E: Unable to connect to Anki. Is it running?".red());
                std::process::exit(1)
            }
        }
    }

    let mut total = 0;
    let mut nonempty = 0;

    for lang in languages.iter() {
        total += lang.count;
        if lang.count > 0 {
            nonempty += 1
        }
    }

    let avg = total as f32/ languages.len() as f32;
    let avg_nonempty = total as f32 / nonempty as f32;

    let avg_f = format!("{:>8.2}", avg);
    let avg_nonempty_f = format!("{:>8.2}", avg_nonempty);

    println!("{:35}{:>5}", "Total cards", total.to_string().blue());
    println!("{:35}{:>8}", "Average", avg_f.blue());
    println!("{:35}{:>8}", "Average (non-empty)", avg_nonempty_f.blue());
    println!("{}", "-".repeat(78));

    languages.sort_by(|a, b| b.count.cmp(&a.count));
    for (i, lang) in languages.iter().enumerate() {
        let perc = lang.count as f32 / total as f32 * 100.0;
        let perc_f = format!("{:>15.1}%", perc);

        let count_f: String = if (lang.count as f32) < avg {
            format!("{:>5}", lang.count.to_string().red())
        } else {
            format!("{:>5}", lang.count.to_string().green())
        };

        let perc_ff: String = if (lang.count as f32) < avg {
            format!("{:>15}", perc_f.red())
        } else {
            format!("{:>15}", perc_f.green())
        };

        println!("{:3}. {:30}{:5}{:15}", i+1, lang.tag, count_f, perc_ff);
    }
}

fn tags_file() -> PathBuf {
    let mut fname = match home_dir() {
        Some(path) => path,
        None => PathBuf::from(""),
    };
    fname.push(TAG_FILE);
    fname
}

fn parse_file(file: File) -> Vec<String> {
    let mut languages: Vec<String> = vec![];
    let reader = BufReader::new(file);
    for line in reader.lines() {
        match line {
            Ok(line) => {
                if !line.trim().is_empty() {
                    languages.push(line.trim().to_string());
                }
            }
            _ => println!("Foo happened!"),
        }
    }

    languages
}

fn anki(tag: &String) -> Result<HashMap<String, Value>, reqwest::Error> {
    let payload = json!({
        "version": 6,
        "action": "findNotes",
        "params": {
            "query": format!("tag:{}", tag)
        }
    });

    // XXX: we should reuse this client
    let client = reqwest::blocking::Client::new();
    Ok(client
        .post("http://localhost:8765")
        .json(&payload)
        .send()?
        .json::<HashMap<String, Value>>()?)
}
